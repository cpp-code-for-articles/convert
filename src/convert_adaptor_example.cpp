//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include "common.h"
#include "convert.h"

//------------------------------------------------
namespace detail {
   //------------------------------------------------
   // Handle pipeline endpoint - range adaptor
   template<typename ConT> //
   struct ConvertTyped : std::ranges::range_adaptor_closure<ConvertTyped<ConT>> {

      template<typename Rng>
      constexpr auto operator()(Rng&& src) {
         return convert<ConT>(src);
      }
   };
   //------------------------------------------------
   // Handle pipeline endpoint - range adaptor
   template<template<typename...> typename Con> //
   struct Convert : std::ranges::range_adaptor_closure<Convert<Con>> {

      template<typename Rng>
      constexpr auto operator()(Rng&& src) {
         return convert<Con>(src);
      }
   };
}
//------------------------------------------------
// handle data | convert()
template<template<typename...> typename Con>
constexpr auto convert() -> detail::Convert<Con> {
   return detail::Convert<Con> { };
}
//------------------------------------------------
// handle data | convert<container>()
template<typename ConT>
constexpr auto convert() {
   return detail::ConvertTyped<ConT> { };
}

//================================================
void range_adaptor_example() {

   auto vecA = std::views::iota(1, 5) //
               | std::views::transform([ ](auto const v) { return v * 2; })  //
               | convert<std::vector>();
   fmt::println("{}", vecA);

   auto lst = vecA | std::views::take(3) | convert<std::list<double>>();
   fmt::println("{}", lst);

   auto example_1 = (std::views::iota(1, 10) //
                     | std::views::transform([ ](auto const v) { return v * 2; })  //
                     | convert<std::vector>() //
                    ) //
                    | std::views::take(5) //
                    | convert<std::list>();
   fmt::println("{}", example_1);
};
