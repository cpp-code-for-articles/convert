//
// Created by rmerriam on 7/3/24.
//
#include <list>
#include <vector>
#include <ranges>

using std::operator ""sv;

constexpr std::string_view data {"ZYXWVU"sv};

// create a vector with the elements of data
auto a = ranges::to<std::vector<char>>(data);

// explicit conversion char -> long
auto b = ranges::to<std::vector<long>>(data);

// deducing value_type
auto c = ranges::to<std::vector>(data);

// Supports converting sequence containers to associative ones
auto d = ranges::to<std::set>(c);

//Supports converting associative container to sequence containers
auto e = ranges::to<std::list>(d);

//Pipe syntaxe
auto f = data | ranges::view::take(3) | ranges::to<std::vector>();

//The pipe syntax also support specifying the type and conversions
auto g = data | ranges::view::take(3) | ranges::to<std::vector<long>>();


auto operator|(range_adaptor_closure&& lhs,
               range_adaptor_closure&& rhs) {...}

auto v = data | convert<vector>();

auto v = rng::operator|(data, convert<con>);
