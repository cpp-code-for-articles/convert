//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================

#pragma once
#include "common.h"
#include "convert.h"
//------------------------------------------------
namespace detail {
   //------------------------------------------------
   // Handle pipeline endpoint - range adaptor
   template<typename ConT> //
   struct ConvertTyped : std::ranges::range_adaptor_closure<ConvertTyped<ConT>> {

      template<typename Rng>
      constexpr auto operator()(Rng&& src) {
         return convert<ConT>(src);
      }
   };
   //------------------------------------------------
   // Handle pipeline endpoint - range adaptor
   template<template<typename...> typename Con> //
   struct Convert : std::ranges::range_adaptor_closure<Convert<Con>> {

      template<typename Rng>
      constexpr auto operator()(Rng&& src) {
         return convert<Con>(src);
      }
   };
}
//------------------------------------------------
// handle data | convert()
template<template<typename  ...> typename Con>
constexpr auto convert() -> detail::Convert<Con> {
   return detail::Convert<Con> { };
}
//------------------------------------------------
// handle data | convert<container>()
template<typename ConT>
constexpr auto convert() {
   return detail::ConvertTyped<ConT> { };
}
