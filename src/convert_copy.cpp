//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================

#include "common.h"

//------------------------------------------------
template<typename ConT, rng::range Rng> //
static auto convert(Rng&& src) -> ConT {
   ConT dst(src.size());
   rng::copy(src, dst.begin());
   return dst;
}

#include "con_test.h"

//------------------------------------------------
void convert_copy() {
   con_test<std::vector<char>>("vector", data);
   con_test<std::forward_list<char>>("forward_list", data);
   con_test<std::list<char>>("list", data);
   con_test<std::deque<char>>("deque", data);

   con_test<std::vector<long>>("vector", data);

#if 0
   // error: class std::set<char>’ has no 'resize' member
   conf_test<std::set<char>>("set", data);
   conf_test<std::multiset<char>>("multiset", data);
   conf_test<std::unordered_set<char>>("unordered_set", data);
   conf_test<std::unordered_multiset<char>>("unordered_multiset", data);
#endif

#if 0
   // error: class std::stack<char>’ has no remember named 'begin'
   conf_test<std::stack<char>>("stack", data);
   conf_test<std::queue<char>>("queue", data);
   conf_test<std::priority_queue<char>>("priority_queue", data);
#endif


};
