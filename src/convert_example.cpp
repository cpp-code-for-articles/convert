//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ranges.h>

#include "convert_adaptor.h"

//------------------------------------------------
constexpr std::string_view input = { //     @formatter:off
   R"(467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..)"
};  //     @formatter:on
//------------------------------------------------
using fmt::println;
using std::get, std::tuple, std::string_view, std::vector;
using vws::split, vws::drop_while, vws::filter, vws::enumerate, //
   vws::transform, vws::take, vws::chunk_by, vws::drop, vws::filter, vws::common;
//================================================
void convert_usage() {
   fmt::println("{}", __PRETTY_FUNCTION__);

   auto split_line = input | split('\n') | enumerate; // | rng::to<vector>();

   fmt::println("{}", fmt::join(split_line, "\n"));
   println("");

   for (auto sl: split_line) {
      auto [line_num, line] = sl;

      fmt::println("{}", fmt::join(line | enumerate, " "));
   }

   //   auto num2 = input | split('\n') | enumerate | rng::to<vector>();
   //
   //   fmt::println("{}", fmt::join(num2, "\n"));
   //   println("");


#include <iostream>
#include <ranges>
#include <string>
#include <vector>

   namespace r = std::ranges;
   namespace rv = std::ranges::views;

   bool is_digit_or_dash(char ch) { return ch == '-' || std::isdigit(ch); }

   std::vector<int> parse_numbers(const std::string& str) {
      return str |  //
             rv::chunk_by([ ](char lhs, char rhs) -> bool {
                return is_digit_or_dash(lhs) && is_digit_or_dash(rhs);
             } //
             ) //
             | //
             rv::filter([ ](auto&& rng) -> bool { //
                return is_digit_or_dash(rng.front());
             } //
             ) | rv::transform([ ](auto&& rng) -> int {
         return std::stoi(rng | r::to<std::string>());
      } //
      ) | r::to<std::vector<int>>();
   }

   int xxx() {
      std::string str = "19, 13, 30 @ -2,  1, -2";
      for (int val: parse_numbers(str)) {
         std::cout << val << '\t';
      }
   }
}
