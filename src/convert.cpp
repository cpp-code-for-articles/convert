//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================

extern void convert_copy();
extern void convert_for();
extern void convert_if_copy_and_for();
extern void convert_copy_and_for();
extern void initialize();
extern void deduce_test();
extern void tuple_test();
extern void pipe_operator();
extern void range_adaptor();
extern void range_adaptor_example();
extern void convert_usage();
//------------------------------------------------
auto main() -> int {
   //   convert_copy();
   //   convert_for();
   //   convert_if_copy_and_for();
   //   convert_copy_and_for();
   //   initialize();
   //      deduce_test();
   //   tuple_test();
   //   pipe_operator();
   //      range_adaptor();
   //   range_adaptor_example();
   convert_usage();

   return 0;
}
