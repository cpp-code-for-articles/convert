//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include "common.h"
#include "convert_adaptor.h"

//================================================
static auto test_a_pipe = [ ](auto pipe, auto const& data) {
   auto vec = data | pipe | convert<std::vector>();

   if constexpr (rng::range<typename decltype(vec)::value_type>) {
      // handle ranges of ranges
      fmt::println("R {}", fmt::join(vec, " "));
   }
   else {
      fmt::println("T {}", vec);
   }
};
//------------------------------------------------
void test_pipes(auto& pipes, auto const& t_data) {
   auto convert_pipes = [&t_data]<typename... Ts>(Ts const& ... a_pipe) {
      ((test_a_pipe(a_pipe, t_data)), ...);
   };
   std::apply(convert_pipes, pipes);
}
//================================================
#include "con_test.h"

//================================================
void range_adaptor() {

   std::tuple pipes_to_test { //
      vws::drop(3), //
      vws::take(3), //
      vws::stride(2), //
      vws::reverse, //

      vws::split('X'), //

      vws::chunk(3), //
      vws::chunk(3) | vws::reverse, //
      vws::chunk(3) | vws::enumerate, //

      vws::split('X') | vws::join | vws::common, //
      vws::split('X') | vws::enumerate | vws::common, //
   };

   test_pipes(pipes_to_test, data);
   fmt::println("");

   con_test<std::deque>("deque", data);
   con_test<std::set>("set", data);
   con_test<std::stack>("stack", data);
   con_test<std::vector>("vector", data);

   con_test<std::vector>("take", data | vws::take(5));
   con_test<std::vector>("chunk", data | vws::chunk(2));
   con_test<std::vector>("split", data | vws::split("W"sv));

   // does a direct call work?
   auto vec1 = convert<std::vector<long>>(data | vws::take(5));
   fmt::println("{}", vec1);

   auto vec2 = convert<std::vector<long>>(data | vws::take(5));
   fmt::println("");

   auto vec3 = data | vws::take(3) | convert<std::vector>();
   fmt::println("{}", vec3);

   auto vec4 = data | vws::take(3) | convert<std::vector<long>>();
   fmt::println("{}", vec4);
   fmt::println("");

   auto vecA = std::views::iota(1, 5) //
               | std::views::transform([ ](auto const v) { return v * 2; })  //
               | convert<std::vector>();
   fmt::println("{}", vecA);

   auto lst = vecA | std::views::take(3) | convert<std::list<double>>();
   fmt::println("{}", lst);

};
