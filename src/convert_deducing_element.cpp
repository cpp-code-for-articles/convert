//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include "common.h"
#include "convert.h"

//------------------------------------------------
#include "con_test.h"

//------------------------------------------------
void deduce_test() {
   con_test<std::deque>("deque", data);
   con_test<std::forward_list>("forward_list", data);
   con_test<std::list>("list", data);
   con_test<std::multiset>("multiset", data);
   con_test<std::priority_queue>("priority_queue", data);
   con_test<std::queue>("queue", data);
   con_test<std::set>("set", data);
   con_test<std::stack>("stack", data);
   con_test<std::unordered_multiset>("unordered_multiset", data);
   con_test<std::unordered_set>("unordered_set", data);
   con_test<std::vector>("vector", data);

   fmt::println("");
   con_test<std::vector>("take", data | vws::take(3));
   con_test<std::queue>("chunk", data | vws::chunk(2));
   con_test<std::vector>("split", data | vws::split("W"sv));
   con_test<std::vector>("split", data | vws::chunk(2));

   auto pipe_test = data | vws::take(3);

   auto a = convert<std::vector<char>>(data | vws::take(3));
   auto b = convert<std::vector<long>>(data | vws::take(3));
   auto c = convert<std::vector>(data | vws::take(3));
   auto d = convert<std::vector>(data | vws::chunk(2));

};
