//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================
#include "common.h"
#include "convert.h"

//------------------------------------------------
namespace detail {
   //------------------------------------------------
   // Handle pipeline endpoint - range adaptor
   template<template<typename...> typename Con> //
   struct Convert : std::ranges::range_adaptor_closure<Convert<Con>> {

      template<typename Rng>
      constexpr auto operator()(Rng&& src) {
         return convert<Con>(src);
      }
   };
   //------------------------------------------------
   template<template<typename...> typename Con>  //
   constexpr detail::Convert<Con> convert { };
}
//------------------------------------------------
// handle data | convert<container>()
template<template<typename  ...> typename Con>
constexpr auto convert() -> detail::Convert<Con> {
   return detail::convert<Con>;
}
//================================================
static auto convert_a_pipe = [ ](auto pipe, auto const& data) {
   auto vec = data | pipe | convert<std::vector>();

   if constexpr (rng::range<typename decltype(vec)::value_type>) {
      // handle ranges of ranges
      fmt::println("R {}", fmt::join(vec, " "));
   }
   else {
      fmt::println("T {}", vec);
   }
};
//------------------------------------------------
void test_pipes(auto& pipes, auto const& test_data) {
   auto p_test = [ ](auto&& pipes, auto const& data) {
      auto convert_pipes = [&data]<typename... Ts>(Ts const& ... a_pipe) {
         ((convert_a_pipe(a_pipe, data)), ...);
      };
      std::apply(convert_pipes, pipes);
   };
   p_test(pipes, test_data);
}
//================================================
#include "con_test.h"

//================================================
void pipe_operator() {
   fmt::println("{}", __PRETTY_FUNCTION__);
   std::tuple pipes_to_test { //
      vws::drop(3), //
      vws::take(3), //
      vws::stride(2), //
      vws::reverse, //

      vws::split('X'), //

      vws::chunk(3), //
      vws::chunk(3) | vws::reverse, //
      vws::chunk(3) | vws::enumerate, //

      vws::split('X') | vws::join | vws::common, //
      vws::split('X') | vws::enumerate | vws::common, //
   };

   test_pipes(pipes_to_test, data);
   fmt::println("");

   con_test<std::deque>("deque", data);
   con_test<std::set>("set", data);
   con_test<std::stack>("stack", data);
   con_test<std::vector>("vector", data);

   con_test<std::vector>("take", data | vws::take(5));
   con_test<std::vector>("chunk", data | vws::chunk(2));
   con_test<std::vector>("split", data | vws::split("W"sv));
   fmt::println("");

   // does a direct call work?
   auto vec1 = convert<std::vector<long>>(data | vws::take(5));
   fmt::println("{}: {}", "vec1 ", vec1);

   auto vec2 = data | vws::take(5) | rng::to<std::vector>();
   fmt::println("{}: {}", "to ", vec2);
   fmt::println("");

   auto times_2 = [ ](auto const v) { return v * 2; };
   {
      auto vec = vws::iota(1, 5) //
                 | vws::transform(times_2 ) //
                 | rng::to<std::vector>();

      auto lst = vec //
                 | vws::take(3) //
                 | rng::to<std::list<double>>();
      fmt::println("{}: {}", "lst ", lst);

      fmt::print("l [");
      for (auto l: lst) {
         fmt::print("{},\t", l);
      }
      fmt::println("]\n");
   }

   {
      auto trans = vws::iota(1, 5) | vws::transform(times_2);
      std::vector vec( rng::cbegin(trans), rng::cend(trans));

      auto tk (vec | vws::take(3));
      std::list lst(rng::cbegin(tk), rng::cend(tk));

      fmt::println("{}: {}", "lst ", lst);

   }

};
