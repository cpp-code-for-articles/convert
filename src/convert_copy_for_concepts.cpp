//==============================================================================
//  Copyright (c) 2024. Mystic Lake Software
//
//  This is free software; you can redistribute it and/or modify it under
//  the terms of the GNU General Public License  as published by the Free
//  Software Foundation; either version 3 of the License, or (at your
//  option) any later version.
//
//  This is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http:www.gnu.org/licenses/>.
//==============================================================================

#include "common.h"

//------------------------------------------------
template<typename ConT, rng::range Rng>
static auto convert(Rng&& src) -> ConT requires requires(ConT c) {
   c.resize(1);
} {
   fmt::print("copy\t");
   ConT dst(src.size());
   rng::copy(src, dst.begin());
   return dst;
}
//------------------------------------------------
template <typename E>
concept Emplaceable = requires(E e) { e.emplace(1); };
//------------------------------------------------
template<Emplaceable ConT, rng::range Rng>
static auto convert(Rng&& src) -> ConT {
   fmt::print("for \t");
   ConT dst;
   for (auto&& s: src) {
      dst.emplace(s);
   }
   return dst;
}
//------------------------------------------------
#include "con_test.h"

//------------------------------------------------
void convert_copy_and_for_concepts() {

   con_test<std::deque<char>>("deque", data);
   con_test<std::forward_list<char>>("forward_list", data);
   con_test<std::list<char>>("list", data);
   con_test<std::multiset<char>>("multiset", data);
   con_test<std::priority_queue<char>>("priority_queue", data);
   con_test<std::queue<char>>("queue", data);
   con_test<std::set<char>>("set", data);
   con_test<std::stack<char>>("stack", data);
   con_test<std::unordered_multiset<char>>("unordered_multiset", data);
   con_test<std::unordered_set<char>>("unordered_set", data);
   con_test<std::vector<char>>("vector", data);

   fmt::println("");
   con_test<std::vector<int>>("vec int", data);
   con_test<std::set<int>>("set int", data);

   fmt::println("");
   con_test<std::vector<char>>("take", data | vws::take(3));
   //   con_test<std::vector<char>>("chunk", data | vws::chunk(2));
   //   con_test<std::vector<char>>("split", data | vws::split("A"sv));
};
