# Convert - a version of ranges::to

## An exploration into writing a version of ranges::to with the name convert.

| Order | File                     | Activity                                           |
|:-----:|:-------------------------|----------------------------------------------------|
|   1   | convert_copy             | using rng::copy                                    |
|   2   | convert_for              | using range-for loop                               |
|   3   | convert_copy_for         | using both copy and range_for_loop                 |
|   4   | convert_initialize       | using initialization instead of copy or loop       |
|   5   | convert_deducing_element | add capability to deduce element from range        |
|   6   | convert_tuple_test       | add ability to test pipelines using tuple          |
|   7   | convert_range_adaptor    | add capability for convert as endpoint of pipeline |
